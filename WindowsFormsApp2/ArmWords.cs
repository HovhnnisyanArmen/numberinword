﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace WindowsFormsApp2
{
    class ArmWords : INumberFormater
    {
        static ArmWords()
        {
            dic = new Dictionary<int, string>()
            {
                { 0, "Զրո" },
                { 1, "Մեկ" },
                { 2, "Երկու" },
                { 3, "Երեք"},
                { 4, "Չորս"},
                { 5 ,"Հինգ"},
                { 6 ,"Վեց"},
                { 7 ,"Յոթ"},
                { 8 ,"Ութ"},
                { 9 ,"Ինն"},
                { 10 ,"Տաս"},
                { 11, "Տասնմեկ" },
                { 12, "Տասներկու" },
                { 13, "Տասներեք"},
                { 14, "Տասնչորս"},
                { 15 ,"Տասնհինգ"},
                { 16 ,"Տասնվեց"},
                { 17 ,"Տասնյոթ"},
                { 18 ,"Տասնութ"},
                { 19 ,"Տասնինն"},
                { 20 ,"Քսան" },
                { 30 ,"Երեսուն" },
                { 40 ,"Քառասուն" },
                { 50 ,"Հիսուն" },
                { 60 ,"Վաթսուն" },
                { 70 ,"Յոթանասուն" },
                { 80 ,"Ութսուն" },
                { 90 ,"Իննսուն" },
                { 100 ,"Հարյուր" },
                { 1000 ,"Հազար" },
                { 1000000 ,"Միլլիոն" }
            };
        }
        private static readonly Dictionary<int, string> dic;
        private string numberInWord;
        

        public string Format(int number)
        {
            string num = Convert.ToString(number);
            int mnacord = 0;
            switch (num.Length)
            {
                case 1:
                    numberInWord += $" {dic[number]}";
                    break;
                case 2:
                    if (number%10==0 || number<20)
                    {
                        numberInWord += $" {dic[number]}";
                        break;
                    }
                    mnacord = number % 10;
                    numberInWord += $" {dic[number - mnacord]}";
                    Format(mnacord);
                    break;
                case 3:
                    if (number % 100 == 0)
                    {
                        numberInWord += $" {dic[number / 100]} {dic[100]}";
                        break;

                    }
                    mnacord = number % 100;
                    numberInWord += $" {dic[number / 100]} {dic[100]}";
                    Format(mnacord);
                    break;
                case 4:
                    if (number % 1000 == 0)
                    {
                        numberInWord += $" {dic[number / 1000]} {dic[1000]}";
                        break;

                    }
                    

                    mnacord = number % 1000;
                    
                    numberInWord += $" {dic[number / 1000]} {dic[1000]}";
                    Format(mnacord);
                    break;
                case 5:
                    if (number % 1000 == 0)
                    {
                        numberInWord += $" {Format(number / 1000)} {dic[1000]}";
                        break;

                    }

                    mnacord = number % 1000;
                    numberInWord += $" {Format(number / 1000)} {dic[1000]}";
                    Format(mnacord);
                    break;
                case 6:
                    if (number % 1000 == 0)
                    {
                        numberInWord += $" {Format(number / 1000)} {dic[1000]}";
                        break;

                    }

                    mnacord = number % 1000;
                    numberInWord += $" {Format(number / 1000)} {dic[1000]}";
                    Format(mnacord);
                    break;

            }


            if(char.IsWhiteSpace(numberInWord[0]))
                numberInWord = numberInWord.Remove(0,1);
            
            return numberInWord;
        }
              
        
    }
}
