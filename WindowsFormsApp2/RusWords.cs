﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp2
{
    class RusWords : INumberFormater
    {

        static RusWords()
        {
            dic = new Dictionary<int, string>()
            {
                { 0, "ноль" },
                { 1, "один" },
                { 2, "два" },
                { 3, "три"},
                { 4, "четыре"},
                { 5 ,"пять"},
                { 6 ,"шесть"},
                { 7 ,"семь"},
                { 8 ,"восемь"},
                { 9 ,"девять"},
                { 10 ,"десять"},
                { 11, "одиннадцать" },
                { 12, "двенадцать" },
                { 13, "тринадцать"},
                { 14, "четырнадцать"},
                { 15 ,"пятнадцать"},
                { 16 ,"шестнадцать"},
                { 17 ,"семнадцать"},
                { 18 ,"восемнадцать"},
                { 19 ,"девятнадцать"},
                { 20 ,"двадцать" },
                { 30 ,"тридцать" },
                { 40 ,"сорок" },
                { 50 ,"пятьдесят" },
                { 60 ,"шестьдесят" },
                { 70 ,"семьдесят" },
                { 80 ,"восемьдесят" },
                { 90 ,"девяносто" },
                { 100 ,"сто" },
                { 200 ,"двести" },
                { 300 ,"триста" },
                { 400 ,"четыреста" },
                { 1000 ,"oдна тысяча" },
                { 2000 ,"две тысячи" },
                { 3000 ,"три тысячи"},
                { 4000 ,"четыре тысячи"},
                { 1000000 ,"миллион" }
            };
        }

        private static readonly Dictionary<int, string> dic;
        private string numberInWord;

        public string Format(int number)
        {
            string num = Convert.ToString(number);
            int mnacord = 0;
            switch (num.Length)
            {
                case 1:
                    numberInWord += $" {dic[number]}";
                    break;
                case 2:
                    if (number%10==0 || number < 20)
                    {
                        numberInWord += $" {dic[number]}";
                        break;
                    }
                    mnacord = number % 10;
                    numberInWord += $" {dic[number - mnacord]}";
                    Format(mnacord);
                    break;
                case 3:
                    if (number<500)
                    {
                        if (number%100 == 0)
                        {
                            numberInWord += $" {dic[number]}";
                            break;
                        }
                        numberInWord += $" {dic[number-number%100]}";
                        Format(number % 100);
                        break;

                    }
                    if (number % 100 == 0)
                    {
                        numberInWord += $" {dic[number/100]}сот";
                        break;
                    }
                    mnacord = number % 100;
                    numberInWord += $" {dic[number / 100]}сот";
                    Format(mnacord);
                    break;
                case  4:
                    
                    if (number < 5000)
                    {
                        if (number % 1000 == 0)
                        {
                            numberInWord += $" {dic[number]}";
                            break;
                        }
                        mnacord = number % 1000;
                        numberInWord += $" {dic[number - mnacord]}";
                        Format(mnacord);
                        break;
                    }
                    if (number % 1000 == 0)
                    {
                        numberInWord += $" {dic[number/1000]} тысяч";
                        break;
                    }
                    mnacord = number % 1000;
                    numberInWord += $" {dic[number / 1000]} тысяч";
                    Format(mnacord);
                    break;
                case 5:
                    if (number <= 20000)
                    {
                        if (number % 1000 == 0)
                        {
                            numberInWord += $" {Format(number / 1000)} тысяч";
                            break;
                        }
                        numberInWord += $" {Format(number/1000)} тысяч";
                        Format(number % 1000);
                        break;
                    }
                    if (number % 10000 < 1000 && number%10000>0)
                    {
                        numberInWord += $" {Format(number / 1000)} тысяч";
                        Format(number % 1000);
                        break;
                    }
                    if (number % 10000 == 0)
                    {
                        numberInWord += $" {dic[number / 1000]} тысяч";
                        break;
                    }
                    int temp = number - number % 10000;
                    numberInWord += $" {dic[temp/1000]}";
                    Format(number % 10000);
                    
                    break;
                case 6:
                    if (number % 100000 == 0)
                    {
                        numberInWord += $" {Format(number/1000)} тысяч";
                        break;
                    }
                    if (number % 10000 < 1000 && number % 10000 > 0)
                    {
                        numberInWord += $" {Format(number / 1000)} тысяч";
                        Format(number % 1000);
                        break;
                    }
                    temp = number - number % 100000;
                    numberInWord += $" {Format(temp / 1000)}";
                    Format(number % 100000);
                    break;




            }

            if (char.IsWhiteSpace(numberInWord[0]))
                numberInWord = numberInWord.Remove(0, 1);

            return numberInWord;
        }
    }
}
